<?php

namespace Sfoon\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class SfoonPlugin implements PluginInterface
{
	public function activate(Composer $composer, IOInterface $io)
	{
		$installer = new SfoonInstaller($io, $composer);

		$composer->getInstallationManager()->addInstaller($installer);
	}
}