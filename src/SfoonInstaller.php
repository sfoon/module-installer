<?php

namespace Sfoon\Composer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class SfoonInstaller extends LibraryInstaller
{
	public $packages = [
		"sfoon-module"
	];

	public function getInstallPath(PackageInterface $package)
	{
		$type = $package->getType();
		$names = $package->getNames();

		if (is_array($names)) {
			$names = $names[0];
		}

		if ("sfoon-module" == $type) {
			list($vendor, $package) = explode("/", $names);

			return "modules/".$vendor."/".$package;
		}
	}

	public function supports($packageType)
	{
		return in_array($packageType, $this->packages);
	}
}
