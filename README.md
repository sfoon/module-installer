<p align="center">
    <img src="https://res.cloudinary.com/dgzk4utc6/image/upload/v1589334424/logo.png" width="300">
</p>

<p align="center">
    <a href="https://gitlab.com/sfoon/framework">
        <img src="https://img.shields.io/badge/License-MIT-yellow.svg">
        <img src="http://ForTheBadge.com/images/badges/uses-git.svg">
        <img src="http://ForTheBadge.com/images/badges/built-by-developers.svg">
    </a>
</p>

## About

This repository contains the Sfoon E-Commerce Module Installer.
